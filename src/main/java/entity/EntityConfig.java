
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author banu
 */
public class EntityConfig {

    private String workDir;
    private String httpPort;
    private String pathUri;
    
    private String dbPgsql;
    private String dbPgsqlIp;
    private String dbPgsqlPort;
    private String dbPgsqlUser;
    private String dbPgsqlPass;
    
    private String dbPgsqlSales;
    private String dbPgsqlIpSales;
    private String dbPgsqlPortSales;
    private String dbPgsqlUserSales;
    private String dbPgsqlPassSales;
    
    private String dbMysql;
    private String dbMysqlIp;
    private String dbMysqlPort;
    private String dbMysqlUser;
    private String dbMysqlPass;
    
    private String ftpIp;
    private String ftpPort;
    private String ftpUser;
    private String ftpPass;
    private String ftpPath;
    
    private String maxThreads;
    private String minThreads;
    private String lowThreads;
    private String maxQueued;
    private String maxTotalConnections;
    private String maxIoIdleTimeMs;
    private String sessionTtl;
    private String timeRemoveBeforeSessionDelete;
    
    private String encKey;
    private String encIv;
    private String encRequest;
    private String encResponse;
    private String encKeyDb;
    
    private String scaleImgSize;
    private String setConnectTimeout;
    private String setReadTimeout;
    private String logStream;
    private String masterFolder;

    private String usernamePurchase;
    private String keyPurchase;
    private String usernamePayment;
    private String keyPayment;

    public EntityConfig() {
    }

    ////////////////////////////////////////////////////////getter/////////////////////////////////////////////

    public String getWorkDir() {
        return this.workDir;
    }

    public String getHttpPort() {
        return this.httpPort;
    }

    //---------------------db postgres---------------
    public String getDbPgsql() {
        return this.dbPgsql;
    }

    public String getDbPgsqlIp() {
        return this.dbPgsqlIp;
    }

    public String getDbPgsqlPort() {
        return this.dbPgsqlPort;
    }

    public String getDbPgsqlUser() {
        return this.dbPgsqlUser;
    }

    public String getDbPgsqlPass() {
        return this.dbPgsqlPass;
    }
    //---------------------end db postgres---------------
    
    //---------------------db postgres---------------
    public String getDbPgsqlSales() {
        return this.dbPgsqlSales;
    }

    public String getDbPgsqlIpSales() {
        return this.dbPgsqlIpSales;
    }

    public String getDbPgsqlPortSales() {
        return this.dbPgsqlPortSales;
    }

    public String getDbPgsqlUserSales() {
        return this.dbPgsqlUserSales;
    }

    public String getDbPgsqlPassSales() {
        return this.dbPgsqlPassSales;
    }
    //---------------------end db postgres---------------
    
    //---------------------db mysql---------------
    public String getDbMysql() {
        return this.dbMysql;
    }

    public String getDbMysqlIp() {
        return this.dbMysqlIp;
    }

    public String getDbMysqlPort() {
        return this.dbMysqlPort;
    }

    public String getDbMysqlUser() {
        return this.dbMysqlUser;
    }

    public String getDbMysqlPass() {
        return this.dbMysqlPass;
    }
    //---------------------end db mysql---------------
    
    //---------------------ftp---------------
    public String getFtpIp() {
        return this.ftpIp;
    }

    public String getFtpPort() {
        return this.ftpPort;
    }

    public String getFtpUser() {
        return this.ftpUser;
    }

    public String getFtpPass() {
        return this.ftpPass;
    }
    
    public String getFtpPath() {
        return this.ftpPath;
    }
    //---------------------end ftp---------------

    public String getMaxThreads() {
        return this.maxThreads;
    }

    public String getMinThreads() {
        return this.minThreads;
    }

    public String getLowThreads() {
        return this.lowThreads;
    }

    public String getMaxQueued() {
        return this.maxQueued;
    }

    public String getMaxTotalConnections() {
        return this.maxTotalConnections;
    }

    public String getMaxIoIdleTimeMs() {
        return this.maxIoIdleTimeMs;
    }

    public String getPathUri() {
        return this.pathUri;
    }
    
    public String getSessionTtl(){
        return this.sessionTtl;
    }
    public String getTimeRemoveBeforeSessionDelete(){
        return this.timeRemoveBeforeSessionDelete;
    }
    

    public String getEncKeyDb() {
        return this.encKeyDb;
    }
    
    public String getEncKey() {
        return this.encKey;
    }

    public String getEncIv() {
        return this.encIv;
    }

    public String getEncRequest() {
        return this.encRequest;
    }

    public String getEncResponse() {
        return this.encResponse;
    }

    public String getScaleImgSize() {
        return this.scaleImgSize;
    }

    public String getSetConnectTimeout() {
        return this.setConnectTimeout;
    }

    public String getSetReadTimeout() {
        return this.setReadTimeout;
    }

    public String getLogStream() {
        return this.logStream;
    }

    public String getMasterFolder() {
        return this.masterFolder;
    }

    //----------------------------------------------base API--------------------------------------
    public String getUsernamePurchase(){
        return this.usernamePurchase;
    }

    public String getKeyPurchase(){
        return this.keyPurchase;
    }

    public String getUsernamePayment(){
        return this.usernamePayment;
    }

    public String getKeyPayment(){
        return this.keyPayment;
    }


    ///////////////////////////////////////////////setter/////////////////////////////////////////////////////////////

    public void setWorkDir(String workDir) {
        this.workDir = workDir;
    }

    public void setHttpPort(String httpPort) {
        this.httpPort = httpPort;
    }

    //------------------------db postgres-------------------
    public void setDbPgsql(String database) {
        this.dbPgsql = database;
    }

    public void setDbPgsqlIp(String dbIp) {
        this.dbPgsqlIp = dbIp;
    }

    public void setDbPgsqlPort(String dbPort) {
        this.dbPgsqlPort = dbPort;
    }

    public void setDbPgsqlUser(String dbUser) {
        this.dbPgsqlUser = dbUser;
    }

    public void setDbPgsqlPass(String dbPass) {
        this.dbPgsqlPass = dbPass;
    }
    //------------------------end db postgres-------------------
    
    //------------------------db postgres-------------------
    public void setDbPgsqlSales(String database) {
        this.dbPgsqlSales = database;
    }

    public void setDbPgsqlIpSales(String dbIp) {
        this.dbPgsqlIpSales = dbIp;
    }

    public void setDbPgsqlPortSales(String dbPort) {
        this.dbPgsqlPortSales = dbPort;
    }

    public void setDbPgsqlUserSales(String dbUser) {
        this.dbPgsqlUserSales = dbUser;
    }

    public void setDbPgsqlPassSales(String dbPass) {
        this.dbPgsqlPassSales = dbPass;
    }
    //------------------------end db postgres-------------------
    
    //------------------------db mysql-------------------
    public void setDbMysql(String database) {
        this.dbMysql = database;
    }

    public void setDbMysqlIp(String dbIp) {
        this.dbMysqlIp = dbIp;
    }

    public void setDbMysqlPort(String dbPort) {
        this.dbMysqlPort = dbPort;
    }

    public void setDbMysqlUser(String dbUser) {
        this.dbMysqlUser = dbUser;
    }

    public void setDbMysqlPass(String dbPass) {
        this.dbMysqlPass = dbPass;
    }
    //------------------------end db mysql-------------------
    
    //------------------------ftp-------------------
    public void setFtpIp(String ftpIp) {
        this.ftpIp = ftpIp;
    }

    public void setFtpPort(String ftpPort) {
        this.ftpPort = ftpPort;
    }

    public void setFtpUser(String ftpUser) {
        this.ftpUser = ftpUser;
    }

    public void setFtpPass(String ftpPass) {
        this.ftpPass = ftpPass;
    }
    
    public void setFtpPath(String ftpPath) {
        this.ftpPath = ftpPath;
    }
    //------------------------end ftp-------------------

    public void setMaxThreads(String maxThreads) {
        this.maxThreads = maxThreads;
    }

    public void setMinThreads(String minThreads) {
        this.minThreads = minThreads;
    }

    public void setLowThreads(String lowThreads) {
        this.lowThreads = lowThreads;
    }

    public void setMaxQueued(String maxQueued) {
        this.maxQueued = maxQueued;
    }

    public void setMaxTotalConnections(String maxTotalConnections) {
        this.maxTotalConnections = maxTotalConnections;
    }

    public void setMaxIoIdleTimeMs(String maxIoIdleTimeMs) {
        this.maxIoIdleTimeMs = maxIoIdleTimeMs;
    }

    public void setPathUri(String pathUri) {
        this.pathUri = pathUri;
    }
    
    public void setSessionTtl(String sessionTtl){
        this.sessionTtl = sessionTtl;
    }
    
    public void setTimeRemoveBeforeSessionDelete(String timeRemoveBeforeSessionDelete){
        this.timeRemoveBeforeSessionDelete = timeRemoveBeforeSessionDelete;
    }
    
    
    public void setEncKeyDb(String encKey) {
        this.encKeyDb = encKey;
    }

    public void setEncKey(String encKey) {
        this.encKey = encKey;
    }

    public void setEncIv(String encIv) {
        this.encIv = encIv;
    }

    public void setEncRequest(String encRequest) {
        this.encRequest = encRequest;
    }

    public void setEncResponse(String encResponse) {
        this.encResponse = encResponse;
    }

    public void setScaleImgSize(String scaleImgSize) {
        this.scaleImgSize = scaleImgSize;
    }

    public void setSetConnectTimeout(String setConnectTimeout) {
        this.setConnectTimeout = setConnectTimeout;
    }

    public void setSetReadTimeout(String setReadTimeout) {
        this.setReadTimeout = setReadTimeout;
    }

    public void setlogStream(String logStream) {
        this.logStream = logStream;
    }

    public void setMasterFolder(String masterFolder) {
        this.masterFolder = masterFolder;
    }

    //------------------------------------------base API--------------------------------------
    public void setUsernamePurchase(String usernamePurchase){
        this.usernamePurchase = usernamePurchase;
    }

    public void setKeyPurchase(String keyPurchase){
        this.keyPurchase = keyPurchase;
    }

    public void setUsernamePayment(String usernamePayment){
        this.usernamePayment = usernamePayment;
    }

    public void setKeyPayment(String keyPayment){
        this.keyPayment = keyPayment;
    }
}
