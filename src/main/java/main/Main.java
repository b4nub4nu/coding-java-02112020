package main;

import controller.ControllerConfig;
import controller.ControllerMasterDataCache;
import entity.EntityConfig;
import helper.Tester;
import helper.Utilities;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.DiskStoreConfiguration;
import org.apache.catalina.connector.Connector;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * @author Banu
 */
@SpringBootApplication
@ComponentScan(basePackages = {"rest_resource"})
public class Main {
    private static EntityConfig conf;

    @Configuration
    public class EmbeddedTomcatConfig {
        //@Value("${http.port}")
        //private int httpPort;

        @Bean
        public EmbeddedServletContainerCustomizer customizeTomcatConnector() {
            return new EmbeddedServletContainerCustomizer() {
                @Override
                public void customize(ConfigurableEmbeddedServletContainer container) {
                    if (container instanceof TomcatEmbeddedServletContainerFactory) {
                        TomcatEmbeddedServletContainerFactory containerFactory = (TomcatEmbeddedServletContainerFactory) container;
                        Connector connector = new Connector(TomcatEmbeddedServletContainerFactory.DEFAULT_PROTOCOL);
                        connector.setPort(Integer.parseInt(Main.conf.getHttpPort()));
                        containerFactory.addAdditionalTomcatConnectors(connector);
                    }
                }
            };
        }
    }

    public static void main(String[] args) throws Exception {
        String workDir = System.getProperty("user.dir");
        System.out.println("Current working directory : " + workDir);
        //print classpath
        System.out.println("main : " + args);
        ClassLoader cl = ClassLoader.getSystemClassLoader();
        URL[] urls = ((URLClassLoader) cl).getURLs();
        for (URL url : urls) {
            System.out.println("classpath : " + url.getFile());
        }
        //setup directory
        Utilities.setupFolder(workDir);

        //----------------get the config data and pass to config entity--------------------
        Main.conf = new EntityConfig();
        ControllerConfig konf = new ControllerConfig(Main.conf, workDir);
        Main.conf = konf.getData();
        //--------------------------ehcache init--------------------------------------
        DiskStoreConfiguration diskStoreConfiguration = new DiskStoreConfiguration();
        diskStoreConfiguration.setPath(workDir + File.separator + "cache");
        // Already created a configuration object ...
        net.sf.ehcache.config.Configuration cacheConfiguration = new net.sf.ehcache.config.Configuration();
        cacheConfiguration.addDiskStore(diskStoreConfiguration);
        //Create a CacheManager using custom configuration
        CacheManager cacheManager = CacheManager.create(cacheConfiguration);
        //--------------------------ehcache save the config entity------------------------
        Cache memoryOnlyCache = new Cache("BanuApp", 1, false, true, 0, 0);
        cacheManager.addCache(memoryOnlyCache);
        //--------------------------get cache and put new element-------------------------------------
        Cache thisCache = cacheManager.getCache("BanuApp");
        thisCache.put(new Element("config", Main.conf));
        //-------------------------ehcache for cookies--------------------------
        //new Cache(workDir, maxElementsInMemory, overflow to disk, eternal, timeToLiveSeconds, timeToIdleSeconds)
        Cache memoryPlusDiskCache = new Cache("BanuAppCookies", 1, true, false, 900, 0);
        cacheManager.addCache(memoryPlusDiskCache);

        //-------------------------ehcache 1 minute expire--------------------------
        Cache memoryPlusDiskCacheNoConcurrent = new Cache("BanuAppNoConcurrent", 100, true, false, 300, 0);
        cacheManager.addCache(memoryPlusDiskCacheNoConcurrent);

        //-------------------------ehcache for dataMaster--------------------------
        //new Cache(workDir, maxElementsInMemory, overflow to disk, eternal, timeToLiveSeconds, timeToIdleSeconds)
        memoryPlusDiskCache = new Cache("BanuAppMaster", 1, true, true, 0, 0);
        cacheManager.addCache(memoryPlusDiskCache);
        //get all master data to save to cache
        ControllerMasterDataCache controllerMasterDataCache = new ControllerMasterDataCache(Main.conf);
        ArrayList<HashMap<String, Object>> rowMsParameter = controllerMasterDataCache.getMsParameter();
        System.out.println("cache ms parameter lenght data : " + rowMsParameter.size());
        //--------------------------get cache of BanuAppMaster and put new element-------------------------
        thisCache = cacheManager.getCache("BanuAppMaster");
       

        //---------------------------springboot-------------------------------------
        HashMap<String, Object> props = new HashMap<>();
        props.put("server.ssl.enabled", true);
        props.put("server.port", Integer.parseInt(Main.conf.getHttpPort()) + 1);
        props.put("server.tomcat.max-connections", Integer.parseInt(Main.conf.getMaxTotalConnections()));
        props.put("server.tomcat.max-threads", Integer.parseInt(Main.conf.getMaxThreads()));
        props.put("server.tomcat.min-spare-threads", Integer.parseInt(Main.conf.getMinThreads()));
        //props.put("server.ssl.key-store", workDir + File.separator + "config" + File.separator + "TMMobileAgent.jks");
        //props.put("server.ssl.key-store-password", "smartcity123");
        //props.put("server.ssl.key-password", "smartcity123");
        ConfigurableApplicationContext server = new SpringApplicationBuilder()
                .sources(Main.class)
                .properties(props)
                .run(args);

        ConfigurableEnvironment env = server.getEnvironment();
        System.out.println("Server Ready At : " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        System.out.println("Http Port : " + Integer.parseInt(Main.conf.getHttpPort()));
        System.out.println("Https Port : " + env.getProperty("server.port"));

        System.out.println("MaxThread : " + env.getProperty("server.tomcat.max-threads"));
        System.out.println("MinThread : " + env.getProperty("server.tomcat.min-spare-threads"));
        System.out.println("MaxConnection : " + env.getProperty("server.tomcat.max-connections"));

        System.out.println("DB IP : " + conf.getDbPgsqlIp());
        System.out.println("DB PORT : " + conf.getDbPgsqlPort());
        System.out.println("DB Name : " + conf.getDbPgsql());
        System.out.println("DB User : " + conf.getDbPgsqlUser());
        System.out.println("DB Password : " + conf.getDbPgsqlPass());

//        SpringApplication.run(main.class, args);
    }
}
