/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import koneksi.DatabaseUtilitiesPgsql;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
//import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;
import javax.crypto.Mac;
import org.apache.commons.codec.binary.Hex;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Encoder;
import java.util.Base64;

/**
 * @author Banu
 */
public class Utilities {

    private Cache cache;
    private EntityConfig entityConfig;
    private final ControllerLog controllerLog;

    public Utilities() {
        CacheManager cacheManager = CacheManager.getInstance();
        //get config cache
        this.cache = cacheManager.getCache("BanuApp");
        Element config = this.cache.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        //getMasterCache to process in module
        this.cache = cacheManager.getCache("BanuAppMaster");
        this.controllerLog = new ControllerLog();
    }

    public boolean moveFile(String to, String fileName) {
        boolean sukses = false;
        String pathTarget = to + fileName;
        try {
            File afile = new File(entityConfig.getWorkDir() + "tmp/" + fileName);
            if (afile.renameTo(new File(pathTarget))) {
                sukses = true;
            } else {
                sukses = false;
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return sukses;
    }

    public String postMultipart(String url, HttpEntity entity) {
        this.controllerLog.logStreamWriter("REQUEST TO : " + url);
        HttpPost post = new HttpPost(url);
        //begin send
        post.setEntity(entity);
        String responseString = "";
        InputStream responseStream = null;
        HttpClient client = new DefaultHttpClient();
        try {
            HttpResponse response = client.execute(post);
            if (response != null) {
                HttpEntity responseEntity = response.getEntity();

                if (responseEntity != null) {
                    responseStream = responseEntity.getContent();
                    if (responseStream != null) {
                        BufferedReader br = new BufferedReader(new InputStreamReader(responseStream));
                        String responseLine = br.readLine();
                        String tempResponseString = "";
                        while (responseLine != null) {
                            tempResponseString = tempResponseString + responseLine + System.getProperty("line.separator");
                            responseLine = br.readLine();
                        }
                        br.close();
                        if (tempResponseString.length() > 0) {
                            responseString = tempResponseString;
                        }
                    }
                }
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        } finally {
            if (responseStream != null) {
                try {
                    responseStream.close();
                } catch (IOException e) {
                    String s = Throwables.getStackTraceAsString(e);
                    controllerLog.logErrorWriter(s);
                }
            }
        }
        client.getConnectionManager().shutdown();
        this.controllerLog.logStreamWriter("RESPONSE FROM : " + url + " : " + responseString);
        return responseString;
    }

    public JSONObject writeLogStreamRequest(String tipe, JSONObject jsonRequest) {
        JSONObject jsonReqForLog = jsonRequest;
        //System.out.println("json req for log : " + jsonReqForLog.toString());
        this.controllerLog.logStreamWriter("Req " + tipe + ": id = " + jsonRequest.get("downlineId").toString() + " ; sourceIp/sport = " + jsonRequest.get("ipAddr").toString() + '/' + jsonRequest.get("port").toString() + "; Agent = " + jsonRequest.get("browserAgent").toString() + "; Body = " + jsonRequest.get("rawReq").toString());
        return jsonReqForLog;
    }

    public JSONObject writeLogStreamResponse(String tipe, JSONObject jsonRequest, String resp) {
        JSONObject jsonReqForLog = new JSONObject();
        try {
            this.controllerLog.logStreamWriter("Resp " + tipe + ": id = " + jsonRequest.get("downlineId").toString() + " ; Response = " + resp);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return jsonReqForLog;
    }

    

    public String getClientIpAddress(HttpServletRequest request) {
        for (String header : Constant.IP_HEADER_CANDIDATES) {
            String ip = request.getHeader(header);
            if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                return ip;
            }
        }
        return request.getRemoteAddr();
    }

    public static void setupFolder(String workDir) {
        //check if folder cache exist
//        File f = new File(workDir + File.separator + "config");
//        if (!f.exists() && !f.isDirectory()) {
//            System.out.println("creating directory: " + f.getName());
//            boolean result = false;
//            try {
//                f.mkdir();
//                result = true;
//            } catch (SecurityException se) {
//                //handle it
//            }
//            if (result) {
//                System.out.println("directory config created");
//            }
//        }

        //check if folder cache exist
        File f = new File(workDir + File.separator + "cache");
        if (!f.exists() && !f.isDirectory()) {
            System.out.println("creating directory: " + f.getName());
            boolean result = false;
            try {
                f.mkdir();
                result = true;
            } catch (SecurityException se) {
                //handle it
            }
            if (result) {
                System.out.println("directory cache created");
            }
        }

        //check if folder cache exist
        f = new File(workDir + File.separator + "logs");
        if (!f.exists() && !f.isDirectory()) {
            System.out.println("creating directory: " + f.getName());
            boolean result = false;
            try {
                f.mkdir();
                result = true;
            } catch (SecurityException se) {
                //handle it
            }
            if (result) {
                System.out.println("directory cache created");
            }
        }

        //check if folder cache exist
        f = new File(workDir + File.separator + "app-logs");
        if (!f.exists() && !f.isDirectory()) {
            System.out.println("creating directory: " + f.getName());
            boolean result = false;
            try {
                f.mkdir();
                result = true;
            } catch (SecurityException se) {
                //handle it
            }
            if (result) {
                System.out.println("directory cache created");
            }
        }
    }

}
