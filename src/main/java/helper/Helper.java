/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import koneksi.DatabaseUtilitiesPgsql;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.FilenameUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

//import bright.express.controller.ControllerLog;

/**
 * @author banu
 */
public class Helper {

    //    ControllerLog ctrlLog;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final Cache cacheConfig;
    private final EntityConfig entityConfig;
    private ControllerEncDec encDec;
    private Utilities utilities;
    private ControllerLog controllerLog;

    public Helper() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("BanuApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    public synchronized String createID() {
        String Id = null;
        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.reset();
            messageDigest.update(Long.toString(System.nanoTime()).getBytes(Charset.forName("UTF8")));
            final byte[] resultByte = messageDigest.digest();
            Id = new String(Hex.encodeHex(resultByte));
        } catch (NoSuchAlgorithmException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return Id;
    }

    public BufferedImage resizeImage(BufferedImage originalImage, int type, int IMG_WIDTH, int IMG_HEIGHT) {
        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();

        return resizedImage;
    }

    public BufferedImage resizeImageWithHint(BufferedImage originalImage, int type, int IMG_WIDTH, int IMG_HEIGHT) {

        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();
        g.setComposite(AlphaComposite.Src);

        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        return resizedImage;
    }

    public String encodeToString(BufferedImage image, String type) {
        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, type, bos);
            byte[] imageBytes = bos.toByteArray();

            BASE64Encoder encoder = new BASE64Encoder();
            imageString = encoder.encode(imageBytes);

            bos.close();
        } catch (IOException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return imageString;
    }

    public String encodeImage(byte[] imageByteArray) {
        return Base64.encodeBase64String(imageByteArray);
    }

    public void createFile(FileItemStream fi, String nameFile) {
        InputStream attachmentStream = null;
        ByteArrayOutputStream out = null;
        FileOutputStream fos = null;
        try {
            attachmentStream = fi.openStream();
            byte[] attachmentBytes;

            //cara 1
//                    try {
//                        attachmentBytes = ByteStreams.toByteArray(attachmentStream);
//                    } finally {
//                        attachmentStream.close();
//                    }
            //cara 2
            out = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024]; // you can configure the buffer size
            int length;
            while ((length = attachmentStream.read(buffer)) != -1) {
                out.write(buffer, 0, length); //copy streams
            }
            attachmentBytes = out.toByteArray();

            fos = new FileOutputStream(entityConfig.getWorkDir() + "tmp" + File.separator + nameFile);
            fos.write(attachmentBytes);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if(attachmentStream != null) {
                    attachmentStream.close();
                }
                if(out != null) {
                    out.flush();
                    out.close();
                }
                if(fos != null) {
                    fos.flush();
                    fos.close();
                }
            } catch (IOException e) {
                String s = Throwables.getStackTraceAsString(e);
                controllerLog.logErrorWriter(s);
            }
        }
    }

    public String getParamMsParameter(String paramName) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        String url = "";
        try {
            //get msparam
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "SELECT * FROM ms_parameter "
                    + "WHERE parameter_name = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, paramName);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                url = rsPgsql.getString("parameter_value");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return url;
    }

    public String requestToGwSocket(JSONObject payloadRequest, String paramName) {
        String paramValue = this.getParamMsParameter(paramName);
        String[] param = paramValue.split(":");
        InetSocketAddress socketAddress = new InetSocketAddress(param[0], Integer.parseInt(param[1]));
        final Socket socket = new Socket();
        final int tTimeout = 1000 * 120;
        final byte cEndMessageByte = -0x01;

        ControllerLog ctrlLog = new ControllerLog();
        ctrlLog.logStreamWriter("Upline -> Req : " + socketAddress.getAddress() + ":" + socketAddress.getPort()
                + " " + payloadRequest.toString());

        String tResponse = "";
        try {
            socket.setSoTimeout(tTimeout);
            socket.connect(socketAddress, tTimeout);

            ByteArrayOutputStream tRequestByteStream = new ByteArrayOutputStream();
            tRequestByteStream.write(payloadRequest.toString().getBytes());
            tRequestByteStream.write(cEndMessageByte);
            socket.getOutputStream().write(tRequestByteStream.toByteArray());

            byte tMessageByte = cEndMessageByte;
            StringBuffer sb = new StringBuffer();
            while ((tMessageByte = (byte) socket.getInputStream().read()) != cEndMessageByte) {
                sb.append((char) tMessageByte);
            }
            tResponse = sb.toString();
            ctrlLog.logStreamWriter("Upline -> Resp : " + tResponse);
        } catch (IOException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return tResponse;
    }
}
