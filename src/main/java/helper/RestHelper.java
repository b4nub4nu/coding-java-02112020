package helper;

import com.github.scribejava.core.model.OAuthConstants;
import com.github.scribejava.core.model.Verb;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class RestHelper {
    JSONObject jsonObject;
    JSONParser parser = new JSONParser();
    Cache cacheConfig;
    Helper hlp;
    private EntityConfig entityConfig;
    private ControllerLog controllerLog;
    private String cookieClient;
    private boolean expireCookie;
    private Utilities utilities;
    private ControllerEncDec encDec;


    public RestHelper(){
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("BanuApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.controllerLog = new ControllerLog();
    }

    public String logRequest(String tipeRequest, JSONObject jsonReq) {
        JSONObject jsonReqForLog = this.utilities.writeLogStreamRequest(tipeRequest, jsonReq);
        //String idTmp = this.utilities.insertTmp(tipeRequest, jsonReqForLog);
        String idTmp = "0";
        return idTmp;
    }

    public void logResponse(String tipeRequest, JSONObject jsonReq, String resp, String idTmp) {
        JSONObject jObjForLog = this.utilities.writeLogStreamResponse(tipeRequest, jsonReq, resp);
        //this.utilities.updateTmp(idTmp, jObjForLog.toString());
    }

}
