/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Constant;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcCRUD;
import koneksi.DatabaseUtilitiesPgsql;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.sql.*;

/**
 * @author banu
 */
public class ImpleCRUD implements InterfcCRUD {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImpleCRUD(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public void saveData(JSONObject jObjCurlReq) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "insert " +
                    " into " +
                    " malakut_banu_hutomo " +
                    " (idRequestBooking, id_platform, nama_platform, doc_type, term_of_payment)  " +
                    " values " +
                    " (?, ?, ?, ?, ?)";
            stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stPgsql.setString(1, jObjCurlReq.get("idRequestBooking").toString());
            stPgsql.setString(2, jObjCurlReq.get("id_platform").toString());
            stPgsql.setString(3, jObjCurlReq.get("nama_platform").toString());
            stPgsql.setString(4, jObjCurlReq.get("doc_type").toString());
            stPgsql.setString(5, jObjCurlReq.get("term_of_payment").toString());
            stPgsql.executeUpdate();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }

    @Override
    public JSONObject getAllData() {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        JSONObject jObjColumn = new JSONObject();
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "select * from malakut_banu_hutomo";
            stPgsql = connPgsql.prepareStatement(query);
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            //convert resultset to jsonArray
            int j = 0;
            while (rsPgsql.next()) {
                for (int i = 1; i <= colCount; i++) {
                    if (rsPgsql.getObject(i) == null) {
                        jObjColumn.put(metaData.getColumnLabel(i), "");
                    } else {
                        jObjColumn.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                    }
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jObjColumn;
    }

    @Override
    public JSONObject getData(JSONObject jsonReq) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        JSONObject jObjColumn = new JSONObject();
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "select * from malakut_banu_hutomo where idRequestBooking = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("idRequestBooking").toString());
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            //convert resultset to jsonArray
            int j = 0;
            while (rsPgsql.next()) {
                for (int i = 1; i <= colCount; i++) {
                    if (rsPgsql.getObject(i) == null) {
                        jObjColumn.put(metaData.getColumnLabel(i), "");
                    } else {
                        jObjColumn.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                    }
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jObjColumn;
    }

    @Override
    public void putData(JSONObject jObjCurlReq) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = " update " +
                    " malakut_banu_hutomo " +
                    " set nama_platform = ? where idRequestBooking = ? ";
            stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stPgsql.setString(1, jObjCurlReq.get("nama_platform").toString());
            stPgsql.setString(2, jObjCurlReq.get("idRequestBooking").toString());
            stPgsql.executeUpdate();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }

    @Override
    public void deleteData(JSONObject jObjCurlReq) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = " delete from" +
                    " malakut_banu_hutomo " +
                    " where idRequestBooking = ? ";
            stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stPgsql.setString(1, jObjCurlReq.get("idRequestBooking").toString());
            stPgsql.executeUpdate();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }
}
