package interfc;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author banu
 */
public interface InterfcCRUD {
    void saveData(JSONObject jObjCurlReq);

    JSONObject getAllData();

    JSONObject getData(JSONObject jsonReq);

    void putData(JSONObject jObjCurlReq);

    void deleteData(JSONObject jObjCurlReq);

}
