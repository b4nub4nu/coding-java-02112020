package interfc;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author banu
 */
public interface InterfcMasterDataCache {

    public ArrayList<HashMap<String, Object>> getMaster(String query);

}
