package rest_resource;

import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.RestHelper;
import helper.Utilities;
//import implement.ImpleCreateBilling;
//import interfc.InterfcCreateBilling;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.*;
import java.net.InetAddress;
import java.net.URL;

@RestController
@RequestMapping("/")
public class ResourceIndex {

    @Value("${database.tipe}")
    String databasePostgre;
    private final EntityConfig entityConfig;
    //private final InterfcCreateBilling service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final ControllerLog controllerLog;
    private RestHelper restHelper;

    public ResourceIndex() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("BanuApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        //this.service = new ImpleCreateBilling(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
        this.restHelper = new RestHelper();
    }

    public JSONObject enhanceRequest(JSONObject jsonReq) {
        String ipAddr = this.utilities.getClientIpAddress(((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest());
        int port = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemotePort();

        String downlineId = this.hlp.createID();
        jsonReq.put("downlineId", downlineId);
        jsonReq.put("ipAddr", ipAddr);
        jsonReq.put("port", port);
        jsonReq.put("browserAgent", "api internal");
        return jsonReq;
    }

    private String getStringIndex() {
        return "<html>" +
                "<head>" +
                "<title>BNI Virtual Account API</title>" +
                "</head>" +
                "<body bgcolor=\"white\">" +
                "<center>API Version : 1.0.0</center>" +
                "<center>Tgl Deploy : 7 July</center>" +
                "<center>Database Postgres : " + databasePostgre + "</center>" +
                "</body>" +
                "</html>";
    }

    @GetMapping("/")
    public String index() {
        return this.getStringIndex();
    }

    @GetMapping("/bniva")
    public String index1() {
        return this.getStringIndex();
    }

    @GetMapping("/bniva/")
    public String index2() {
        return this.getStringIndex();
    }

    @GetMapping("/ip/public")
    @ResponseBody
    public String getIp() {
        String IP = "";
        URL whatismyip = null;
        BufferedReader in = null;
        try {
            whatismyip = new URL("http://checkip.amazonaws.com");
            in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
            IP = in.readLine(); //you get the IP as a String
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return IP;
    }

    @GetMapping("/ip/private")
    @ResponseBody
    public String getIpPrivate() {
        String IP = "";
        try {
            InetAddress IpAdd = InetAddress.getLocalHost();
            IP = IpAdd.getHostAddress();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return IP;
    }

}
