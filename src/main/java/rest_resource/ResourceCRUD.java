package rest_resource;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import controller.ControllerCRUD;
import entity.EntityConfig;
import helper.Helper;
import helper.RestHelper;
import helper.Utilities;
//import implement.ImpleInquiry;
//import interfc.InterfcInquiry;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;
import org.json.XML;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.ByteArrayInputStream;

@RestController
@RequestMapping("/")
public class ResourceCRUD{

    @Value("${database.tipe}")
    String databasePostgre;
    private final EntityConfig entityConfig;
    //private final InterfcCreateBilling service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final ControllerLog controllerLog;
    private RestHelper restHelper;

    public ResourceCRUD() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("BanuApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        //this.service = new ImpleCreateBilling(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
        this.restHelper = new RestHelper();
    }

    public JSONObject enhanceRequest(JSONObject jsonReq) {
        String ipAddr = this.utilities.getClientIpAddress(((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest());
        int port = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemotePort();

        String downlineId = this.hlp.createID();
        jsonReq.put("downlineId", downlineId);
        jsonReq.put("ipAddr", ipAddr);
        jsonReq.put("port", port);
        jsonReq.put("browserAgent", "api internal");
        return jsonReq;
    }

    public org.json.JSONObject convertToJSONObject(String dataRequest) {
        org.json.JSONObject jsonReq = new org.json.JSONObject();
        try {
            jsonReq = new org.json.JSONObject(dataRequest);
        } catch (Exception ex) {
            //String s = Throwables.getStackTraceAsString(ex);
            //controllerLog.logErrorWriter(s);
        }

        try {
            SAXBuilder saxBuilder = new SAXBuilder();
            org.jdom2.Document doc = saxBuilder.build(new ByteArrayInputStream(dataRequest.getBytes("UTF-8")));
            jsonReq = XML.toJSONObject(new XMLOutputter().outputString(doc));
        } catch (Exception ex) {
            //String s = Throwables.getStackTraceAsString(ex);
            //controllerLog.logErrorWriter(s);
        }
        return jsonReq;
    }

    @PostMapping("/bn/postdata")
    @ResponseBody
    public String doPostData(@RequestBody String request) {
        return getString1(request);
    }
    
    @GetMapping("/bn/getalldata")
    @ResponseBody
    public String doGetAllData() {
        return getString2();
    }

    @GetMapping("/bn/getdata/{id}")
    @ResponseBody
    public String doGetData(@PathVariable(value = "id") String dataId) {
        return getString3(dataId);
    }

    @PutMapping("/bn/putdata/{id}")
    @ResponseBody
    public String doUpdateData(@RequestBody String request, @PathVariable(value = "id") String dataId) {
        return getString4(request,dataId);
    }

    @DeleteMapping("/bn/deletedata/{id}")
    @ResponseBody
    public String doDeleteData(@PathVariable(value = "id") String dataId) {
        return getString5(dataId);
    }

    private String getString1(String dataRequest) {
        String resp = "";
        org.json.JSONObject jsonReqOrg = this.convertToJSONObject(dataRequest);
        jsonReqOrg.put("rawReq", dataRequest);
        try {
            JSONObject jsonReq = (JSONObject) new JSONParser().parse(jsonReqOrg.toString());
            jsonReq = this.enhanceRequest(jsonReq);
            //System.out.println("coba = "+jsonReq);
            String tipeRequest = "postData";
            String idTmp = restHelper.logRequest(tipeRequest, jsonReq);
            ControllerCRUD ControllerCRUD = new ControllerCRUD();
            resp = ControllerCRUD.processReqPost(jsonReq);

            if (!tipeRequest.equals("") && !idTmp.equals("")) {
                restHelper.logResponse(tipeRequest, jsonReq, resp, idTmp);
            }
            
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }

        return resp;
    }

    private String getString2() {
        String resp = "";
        org.json.JSONObject jsonReqOrg = this.convertToJSONObject("{}");
        jsonReqOrg.put("rawReq", "{}");
        try {
            JSONObject jsonReq = (JSONObject) new JSONParser().parse(jsonReqOrg.toString());
            jsonReq = this.enhanceRequest(jsonReq);
            String tipeRequest = "getallData";
            String idTmp = restHelper.logRequest(tipeRequest, jsonReq);
            ControllerCRUD ControllerCRUD = new ControllerCRUD();
            resp = ControllerCRUD.processReqGetAll();

            if (!tipeRequest.equals("") && !idTmp.equals("")) {
                restHelper.logResponse(tipeRequest, jsonReq, resp, idTmp);
            }
            
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }

        return resp;
    }

    private String getString3(String dataId) {
        String resp = "";
        org.json.JSONObject jsonReqOrg = this.convertToJSONObject("{}");
        jsonReqOrg.put("rawReq", "{}");
        try {
            JSONObject jsonReq = (JSONObject) new JSONParser().parse(jsonReqOrg.toString());
            jsonReq = this.enhanceRequest(jsonReq);
            String tipeRequest = "getData";
            String idTmp = restHelper.logRequest(tipeRequest, jsonReq);
            ControllerCRUD ControllerCRUD = new ControllerCRUD();
            resp = ControllerCRUD.processReqgetData(jsonReq,dataId);

            if (!tipeRequest.equals("") && !idTmp.equals("")) {
                restHelper.logResponse(tipeRequest, jsonReq, resp, idTmp);
            }
            
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }

        return resp;
    }

    private String getString4(String dataRequest, String dataId) {
        String resp = "";
        org.json.JSONObject jsonReqOrg = this.convertToJSONObject(dataRequest);
        jsonReqOrg.put("rawReq", dataRequest);
        try {
            JSONObject jsonReq = (JSONObject) new JSONParser().parse(jsonReqOrg.toString());
            jsonReq = this.enhanceRequest(jsonReq);
            String tipeRequest = "putData";
            String idTmp = restHelper.logRequest(tipeRequest, jsonReq);
            ControllerCRUD ControllerCRUD = new ControllerCRUD();
            jsonReq.put("idRequestBooking",dataId);
            resp = ControllerCRUD.processReqputData(jsonReq,dataId);

            if (!tipeRequest.equals("") && !idTmp.equals("")) {
                restHelper.logResponse(tipeRequest, jsonReq, resp, idTmp);
            }
            
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }

        return resp;
    }

    private String getString5(String dataId) {
        String resp = "";
        org.json.JSONObject jsonReqOrg = this.convertToJSONObject("{}");
        jsonReqOrg.put("rawReq", "{}");
        try {
            JSONObject jsonReq = (JSONObject) new JSONParser().parse(jsonReqOrg.toString());
            jsonReq = this.enhanceRequest(jsonReq);
            String tipeRequest = "deleteData";
            String idTmp = restHelper.logRequest(tipeRequest, jsonReq);
            ControllerCRUD ControllerCRUD = new ControllerCRUD();
            resp = ControllerCRUD.processReqdeleteData(jsonReq,dataId);

            if (!tipeRequest.equals("") && !idTmp.equals("")) {
                restHelper.logResponse(tipeRequest, jsonReq, resp, idTmp);
            }
            
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }

        return resp;
    }
}
