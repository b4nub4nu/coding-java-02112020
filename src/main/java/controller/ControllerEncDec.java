/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.common.base.Throwables;
import implement.ImpleEncryptDecrypt;
import interfc.InterfcEncryptDecrypt;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

/**
 *
 * @author banu
 */
public class ControllerEncDec {

    InterfcEncryptDecrypt service;
    private String decrypt;
    private byte[] encrypt;
    private ControllerLog controllerLog;

    public ControllerEncDec(String iv, String key) {
        this.service = new ImpleEncryptDecrypt(iv, key);
        this.controllerLog = new ControllerLog();
    }

    public String getDecrypt(byte[] decodedBytes) throws ClassNotFoundException, Exception {
        this.decrypt = this.service.decrypt(decodedBytes);
        return this.decrypt;
    }

    public byte[] getEncrypt(String str) throws ClassNotFoundException, Exception {
        this.encrypt = this.service.encrypt(str);
        return this.encrypt;
    }
    
    //--------------------dari sistem sebelumnya, untuk enc dec pin di database

    private byte[] konversiKeByte(String Kunci) {
        byte[] array_byte = new byte[32];
        int i = 0;
        while (i < Kunci.length()) {
            array_byte[i] = (byte) Kunci.charAt(i);
            i++;
        }
        if (i < 32) {
            while (i < 32) {
                array_byte[i] = (byte) i;
                i++;
            }
        }
        return array_byte;
    }

    private Key generateKey(String kunciEnkripsi) throws Exception {
        Key key = new SecretKeySpec(konversiKeByte(kunciEnkripsi), "AES");
        return key;
    }

    public String encryptDbVal(String text, String Kunci) throws Exception {
        // SecretKeySpec spec = getKeySpec();
        Key key = generateKey(Kunci);
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        BASE64Encoder enc = new BASE64Encoder();
        return enc.encode(cipher.doFinal(text.getBytes())).toString();
    }

    public String decryptDbVal(String text, String Kunci) throws Exception {
        // SecretKeySpec spec = getKeySpec();
        String result = null;
        try {
            Key key = generateKey(Kunci);
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            BASE64Decoder dec = new BASE64Decoder();
            result = new String(cipher.doFinal(dec.decodeBuffer(text)));
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            this.controllerLog.logErrorWriter(s);
        } finally {
            // optional, use this block if necessary
        }

        return result;
    }
}
