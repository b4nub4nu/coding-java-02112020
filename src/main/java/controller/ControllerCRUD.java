package controller;

import com.google.common.base.Throwables;
import entity.EntityConfig;
import helper.Constant;
import helper.Helper;
import helper.Utilities;
import implement.ImpleCRUD;
import interfc.InterfcCRUD;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.text.DateFormat;
import java.util.Calendar;

import java.util.HashMap;
import java.util.Map;

import java.util.concurrent.TimeUnit;
/**
 * @author banu
 */
public class ControllerCRUD {

    private final EntityConfig entityConfig;
    private final InterfcCRUD service;
    private Cache cache;
    private Element elem;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ControllerCRUD() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cache = cacheManager.getCache("BanuApp");
        Element config = this.cache.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.cache = cacheManager.getCache("");
        this.service = new ImpleCRUD(this.entityConfig);
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    //1
    public String processReqPost(JSONObject jsonReq) {
        String resp = "";
        JSONObject jsonResp = new JSONObject();
        JSONObject jObjCurlReq = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "NOK");

        try {
            jsonReq = (JSONObject) new JSONParser().parse(jsonReq.get("rawReq").toString());
           
            //save to db
            jObjCurlReq.put("idRequestBooking", jsonReq.get("idRequestBooking").toString());
            jObjCurlReq.put("id_platform", jsonReq.get("id_platform").toString());
            jObjCurlReq.put("nama_platform", jsonReq.get("nama_platform").toString());
            jObjCurlReq.put("doc_type", jsonReq.get("doc_type").toString());
            jObjCurlReq.put("term_of_payment", jsonReq.get("term_of_payment").toString());

            this.service.saveData(jObjCurlReq);
            
            jsonResp.put("ACK", "OK");
            jsonResp.put("pesan", "OK");

            resp = jsonResp.toString();

        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            resp = jsonResp.toString();
        }
        return resp;
    }

    //2
    public String processReqGetAll() {
        String resp = "";
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "NOK");

        try {

            JSONObject jsonRespCurl = this.service.getAllData();
            
            jsonRespCurl.put("ACK", "OK");
            jsonRespCurl.put("pesan", "OK");

            resp = jsonRespCurl.toString();

        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            resp = jsonResp.toString();
        }
        return resp;
    }

    //3
    public String processReqgetData(JSONObject jsonReq,String dataId) {
        String resp = "";
        JSONObject jsonResp = new JSONObject();
        JSONObject jObjCurlReq = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "NOK");

        try {
            jsonReq = (JSONObject) new JSONParser().parse(jsonReq.get("rawReq").toString());
           
            //save to db
            jObjCurlReq.put("idRequestBooking", dataId);

            JSONObject jsonRespCurl = this.service.getData(jObjCurlReq);
            
            jsonRespCurl.put("ACK", "OK");
            jsonRespCurl.put("pesan", "OK");

            resp = jsonRespCurl.toString();

        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            resp = jsonResp.toString();
        }
        return resp;
    }

    //4
    public String processReqputData(JSONObject jsonReq, String dataId) {
        String resp = "";
        JSONObject jsonResp = new JSONObject();
        JSONObject jObjCurlReq = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "NOK");

        try {
            jsonReq = (JSONObject) new JSONParser().parse(jsonReq.get("rawReq").toString());

            jObjCurlReq.put("nama_platform", jsonReq.get("nama_platform").toString());
            jObjCurlReq.put("idRequestBooking", dataId);

            this.service.putData(jObjCurlReq);
            
            jsonResp.put("ACK", "OK");
            jsonResp.put("pesan", "OK");

            resp = jsonResp.toString();

        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            resp = jsonResp.toString();
        }
        return resp;
    }

    //5
    public String processReqdeleteData(JSONObject jsonReq, String dataId) {
        String resp = "";
        JSONObject jsonResp = new JSONObject();
        JSONObject jObjCurlReq = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "NOK");

        try {
            jsonReq = (JSONObject) new JSONParser().parse(jsonReq.get("rawReq").toString());

            jObjCurlReq.put("idRequestBooking", dataId);

            this.service.deleteData(jObjCurlReq);
            
            jsonResp.put("ACK", "OK");
            jsonResp.put("pesan", "OK");

            resp = jsonResp.toString();

        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            resp = jsonResp.toString();
        }
        return resp;
    }

}
